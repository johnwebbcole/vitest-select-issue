// @vitest-environment jsdom
import { mount } from '@vue/test-utils'
import { expect, it } from 'vitest'
import Foo from './Foo.vue'

it('mount component', async () => {
  expect(Foo).toBeTruthy()

  const wrapper = mount(Foo, {
    props: {
      items: [1, 2, 3, 4],
    },
  })

  expect(wrapper.html()).toMatchInlineSnapshot(`
    "<dir><select>
        <option value=\\"1\\"></option>
        <option value=\\"2\\"></option>
        <option value=\\"3\\"></option>
        <option value=\\"4\\"></option>
      </select></dir>"
  `)
})

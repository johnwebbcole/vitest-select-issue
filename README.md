# Vite/Vitest/happy-dom Issue

With a component that has a array of numbers, then creating a select
list from that array, an error is thrown.

```shell
 FAIL  src/components/Foo-happy-dom.spec.js > mount component
TypeError: value.trim is not a function
 ❯ Function.sanitize node_modules/happy-dom/lib/nodes/html-option-element/HTMLOptionElementValueSanitizer.js:14:22
 ❯ HTMLOptionElement.set value [as value] node_modules/happy-dom/lib/nodes/html-option-element/HTMLOptionElement.js:107:86
 ❯ patchDOMProp node_modules/@vue/runtime-dom/dist/runtime-dom.cjs.js:250:22
 ❯ patchProp node_modules/@vue/runtime-dom/dist/runtime-dom.cjs.js:412:9
 ❯ mountElement node_modules/@vue/runtime-core/dist/runtime-core.cjs.js:5095:21
 ❯ processElement node_modules/@vue/runtime-core/dist/runtime-core.cjs.js:5055:13
 ❯ patch node_modules/@vue/runtime-core/dist/runtime-core.cjs.js:4975:21
 ❯ mountChildren node_modules/@vue/runtime-core/dist/runtime-core.cjs.js:5163:13
 ❯ processFragment node_modules/@vue/runtime-core/dist/runtime-core.cjs.js:5335:13
 ❯ patch node_modules/@vue/runtime-core/dist/runtime-core.cjs.js:4971:17
```

It appears that using JSDOM with vitest does not have the same issue.

See:

- [Foo-happy-dom.spec.js](./src/components/Foo-happy-dom.spec.js)
- [Foo-jsdom.spec.js](./src/components/Foo-jsdom.spec.js)

## Install

`npm install`

## Test

`npm run test`

## Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)
